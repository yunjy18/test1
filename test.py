print("팩맨 게임을 시작합니다.")                        #게임시작 문구
name = input("이름을 입력하세요.: ")                    #이름 적기
print(name,"님 반갑습니다. 직업을 선택해주세요.")
print("1.전사 2.마법사 3.도적")                         #직업 고르기
number = int(input("숫자를 입력해주세요.:")) 
if number == 1:
    print(name,"전사")
if number == 2:
    print(name,"마법사")
if number == 3:
    print(name,"도적")
print("직업 선택이 완료되었습니다.\n")
print(name,"용사님 마을에 오신것을 환영합니다.")
print("모험을 떠나시겠습니까?")
print("1.예 2.아니오")                                #모험을 떠날것인지 정하기
number = int(input("숫자를 입력해주세요.: ")) 
if number == 1:
    print("\n무시무시한 유령이 나타났다!!!")           #모험을 떠나면 유령이 나타남
    print("1.싸운다. 2.도망간다. 3.가만히 있는다.")    #유령에게서 어떻게 할건지 정하기
    number = int(input("숫자를 입력해주세요.: "))
    if number == 1:
        print("유령을 무찔렀습니다.\n")
        import random
        list_a = ['강력한 무기','덜 강력한 무기','약한 무기']   #유령을 무찌르면 랜덤해서 무기를 얻음
        A = random.choice(list_a)
        print(A,"을(를) 획득하였습니다.")
        print("획득한 무기와 그 외 무기의 공격력도 확인할 수 있습니다.")  
        print("------------------------------------------------------")      
        dict = {'강력한 무기':'100','덜 강력한 무기':'50','약한 무기':'10'}       
        for key in dict.keys():
            print(key)
        print("------------------------------------------------------")    
        b = input("공격력이 궁금한 무기의 이름을 입력하시오.: ")   #무기이름을 검색하면 공격력이 나옴
        if b in dict:
            print("{0}의 공격력은 {1}입니다.".format(b,dict[b]))
        else:
            print("목록에 없거나 띄어쓰기를 해서 입력하세요.")  #목록에 없거나 띄어쓰기 안하면 안됨 
    if number == 2:
        print("유령에게서 도망쳤습니다.")                      
        print("당신은 용사의 자격이 없습니다.\nGame Over")
    if number == 3:
        print("유령에게 죽었습니다.\nGame Over")
elif number == 2:
    print("미구현 끝")               #모험을 떠나지 않으면 끝


        